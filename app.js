import axios from 'axios';
import dotenv from 'dotenv';
import fs from 'fs';
import ip from 'ip';
import os from 'os';
import PropertiesReader from 'properties-reader';

import FCSAgent from './classes/FCSAgent.js';
import MQTTHelper from './classes/MQTTHelper.js';
import setPrototypes from './utils/prototypes.js';
import sleep from './utils/sleep.js';

// #region 설정 확인
// config 파일 존재하지 않는 경우
if (!fs.existsSync('.robot.cfg')) {
  console.log('Error: Config file(.robot.cfg) is not exist. Should run setup.sh first.');
  process.exit(0);
}

// config 파일 읽기 및 값 추출
const prop = PropertiesReader('.robot.cfg');
const appId = prop.get('APP_ID');
const robotId = prop.get('ROBOT_ID');
const protocol = prop.get('PROTOCOL');
const udpPort = prop.get('UDP_PORT');
const tcpPort = prop.get('TCP_PORT');
const serialPath = prop.get('SERIAL_PATH');
const baudRate = prop.get('BAUD_RATE');

// 필수 값 존재하지 않는 경우
if (!appId || !robotId || !protocol) {
  console.log('Error: Invalid configuration, please run setup.sh');
  process.exit(0);
}
// #endregion

// #region 환경변수 정의
let nodeEnv = process.env.NODE_ENV;
if (nodeEnv === 'production') {
  nodeEnv = `production.${appId}`;
}

dotenv.config({ path: `./env/.env.${nodeEnv ?? 'development'}` });
// #endregion

// prototype 기능 추가
setPrototypes();

const bootstrap = async () => {
  const connection = {
    http: false,
    mqtt: false,
    fcs: false,
  };

  let robot;
  let mqtt;
  let mqttHelper;

  // #region HTTP 연결
  while (!connection.http) {
    try {
      // 로봇 검증
      [robot, mqtt] = await validateRobot(robotId);
      connection.http = true;

      console.log('[HTTP] connected and validated');
      console.log('- Robot ID\t:', robot.id);
      console.log('- Robot Name\t:', robot.name);
      console.log('- Model ID\t:', robot.model.id);
      console.log('- Model Name\t:', robot.model.name);
    } catch (e) {
      console.log(`[HTTP] ${e.message}`);
      await sleep(3);
    }
  }
  // #endregion

  // #region MQTT 연결
  while (!connection.mqtt) {
    try {
      mqttHelper = new MQTTHelper(mqtt, robotId);
      connection.mqtt = true;
    } catch (e) {
      await sleep(3);
    }
  }
  // #endregion

  // #region FCS 연결
  while (!connection.fcs) {
    try {
      const fcsAgent = new FCSAgent({ protocol, udpPort, tcpPort, serialPath, baudRate });
      fcsAgent.onMessage(mqttHelper.publish);
      mqttHelper.onMessage(fcsAgent.send);
      connection.fcs = true;

      console.log('[FCS] connected');
    } catch (e) {
      console.log(`[FCS] ${e.message}`);
      await sleep(1);
    }
  }
  // #endregion
};

const validateRobot = async (robotId) => {
  const result = await axios.get(`${process.env.API_URL}/robots/${robotId}/mavlink`, {
    data: {
      osversion: os.version(),
      platform: os.platform(),
      hostname: os.hostname(),
      ip: ip.address(),
      isPublicIp: ip.isPublic(ip.address()),
    },
  });
  const { robot, mqtt } = result.data;
  return [robot, mqtt];
};

bootstrap();

process.on('SIGINT', () => {
  console.log('EVENT SIGINT');
  return process.exit(0);
});
