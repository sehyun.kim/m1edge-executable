#!/bin/bash

sleep 30s

if [ ! -e ".robot.cfg" ] ; then
  echo "No configuration. Please tun setup.sh first."
  exit 0
fi

git pull

git reset --hard origin/master

npm install

npm run start

pm2 save