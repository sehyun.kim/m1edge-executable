#!/bin/bash

logo=\
"@@\      @@\   @@\   @@@@@@@@\      @@\                     "$'\n'\
"@@@\    @@@ |@@@@ |  @@  _____|     @@ |                    "$'\n'\
"@@@@\  @@@@ |\_@@ |  @@ |      @@@@@@@ | @@@@@@\   @@@@@@\  "$'\n'\
"@@\@@\@@ @@ |  @@ |  @@@@@\   @@  __@@ |@@  __@@\ @@  __@@\ "$'\n'\
"@@ \@@@  @@ |  @@ |  @@  __|  @@ /  @@ |@@ /  @@ |@@@@@@@@ |"$'\n'\
"@@ |\@  /@@ |  @@ |  @@ |     @@ |  @@ |@@ |  @@ |@@   ____|"$'\n'\
"@@ | \_/ @@ |@@@@@@\ @@@@@@@@\\@@@@@@@ |\@@@@@@@ |\@@@@@@@\ "$'\n'\
"\__|     \__|\______|\________|\_______| \____@@ | \_______|"$'\n'\
"                                        @@\   @@ |          "$'\n'\
"                                        \@@@@@@  |          "$'\n'\
"                                         \______/           "$'\n'

# whiptail 메뉴 선택이 중단되었는지 체크
handleExitStatus() {
    if [ $? -ne 0 ]; then
        echo "Configuration canceled, entered data was not saved."
        exit 0
    fi
}

# Application ID Menu Items
ApplicationIds=(1 "M1UCS"
                2 "Smartii")

# Protocol Menu Items
Protocols=(1 "UDP"
           2 "TCP"
           3 "Serial")

# Device Path Menu Items
Devices=()
while IFS= read -r link; do
    deviceInfo=$(udevadm info --query=all --name="$link")

    usbModel=$(echo "$deviceInfo" | grep -Po 'E: ID_USB_MODEL=.+' | cut -d= -f2)
    vendorName=$(echo "$deviceInfo" | grep -Po 'E: ID_USB_VENDOR=.+' | cut -d= -f2)
    Devices+=("$link" ": $usbModel($vendorName)")
done <<< $(find /dev \( -name 'ttyACM*' -o -name 'ttyUSB*' \))

# 1. App Id 선택
APPID_CHOICE=$(whiptail --title "Application Id" --menu "Choose an Application Id" 15 60 4 \
"${ApplicationIds[@]}" 3>&1 1>&2 2>&3)
handleExitStatus $?

case $APPID_CHOICE in
    1) appId="m1ucs";;
    2) appId="smartii";;
esac

# 2. Robot Id 입력
robotId=$(whiptail --title "Robot Id" --inputbox "Enter Robot Id" 10 60 3>&1 1>&2 2>&3)
handleExitStatus $?

# 3. Protocol 선택
PROTOCOL_CHOICE=$(whiptail --title "Protocol" --menu "Choose a protocol" 15 60 4 \
"${Protocols[@]}" 3>&1 1>&2 2>&3)
handleExitStatus $?

# 3-1. Protocol 세부 선택(Port, DevPath, BaudRate)
case $PROTOCOL_CHOICE in
    1) protocol="udp"
    udpPort=$(whiptail --title "UDP Port" --inputbox "Enter UDP Port" 10 60 14550 3>&1 1>&2 2>&3);;
    2) protocol="tcp"
    tcpPort=$(whiptail --title "TCP Port" --inputbox "Enter TCP Port" 10 60 14550 3>&1 1>&2 2>&3);;
    3) protocol="serial"
        DEVICE_CHOICE=$(whiptail --title "Serial Device Path" --menu "Choose a serial device", 15 60 4 \
        "${Devices[@]}" 3>&1 1>&2 2>&3)
    serialPath=$(echo "$DEVICE_CHOICE" | cut -d" " -f1)
    baudRate=$(whiptail --title "Baud Rate" --inputbox "Enter Baud Rate" 10 60 115200 3>&1 1>&2 2>&3);;
esac
handleExitStatus $?

# 4. .robot.cfg 저장            
if [ -e ".robot.cfg" ] ; then
    rm .robot.cfg
fi

echo "APP_ID="$appId >> .robot.cfg
echo "ROBOT_ID="$robotId >> .robot.cfg
echo "PROTOCOL="$protocol >> .robot.cfg
echo "UDP_PORT="$udpPort >> .robot.cfg
echo "TCP_PORT="$tcpPort >> .robot.cfg
echo "SERIAL_PATH="$serialPath >> .robot.cfg
echo "BAUD_RATE="$baudRate >> .robot.cfg

echo ""
echo "$logo"
echo ""
echo "Configuration successfully completed."