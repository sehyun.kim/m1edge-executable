const setPrototypes = () => {
  // BigInt stringify 처리
  BigInt.prototype['toJSON'] = function () {
    return this.toString();
  };
};

export default setPrototypes;
