import { minimal, MavLinkProtocolV2 } from 'node-mavlink';

const heartbeat = () => {
  // Define properties for heartbeat
  const heartbeat = new minimal.Heartbeat();
  heartbeat.type = 6;
  heartbeat.autopilot = 8;
  heartbeat.baseMode = 192;
  heartbeat.customMode = 0;
  heartbeat.systemStatus = 4;
  heartbeat.mavlinkVersion = 3;

  const mavLink = new MavLinkProtocolV2(255, 190);
  return mavLink.serialize(heartbeat, 0);
};

export default heartbeat;
