import net from 'net';
import { MavLinkPacketSplitter } from 'node-mavlink';
import stream from 'node:stream';

import heartbeat from '../utils/heartbeat.js';

export default class TCPHelper {
  constructor(port) {
    this.input = new stream.PassThrough();
    this.server = net.createServer();
    this.server.listen(port);
  }

  onMessage = (callback) => {
    const reader = this.input.pipe(new MavLinkPacketSplitter());
    reader.on('data', callback);

    this.server.on('connection', (socket) => {
      this.client = socket;

      // TCP 연결 후 heartbeat 메시지 전송
      this.send(heartbeat());

      socket.on('data', (data) => {
        this.input.write(data, 'hex');
      });
    });
    this.server.on('error', this.#onError);
  };

  send = (buffer) => this.client.write(buffer, this.#onSendError);

  #onError = (error) => {
    console.log(`[TCP] OnError: ${error}`);
    this.server.close();
  };

  #onSendError = (error) => {
    if (error) {
      console.log(`[TCP] OnSendError: ${error}`);
    }
  };
}
