import SerialPortHelper from './SerialPortHelper.js';
import TCPHelper from './TCPHelper.js';
import UDPHelper from './UDPHelper.js';

const PROTOCOL = {
  UDP: 'udp',
  TCP: 'tcp',
  SERIAL: 'serial',
};

export default class FCSAgent {
  constructor(options) {
    this.options = options;

    this.#init();
  }

  /**
   * Explicit call of initialization is unnecessary
   */
  #init() {
    switch (this.options.protocol) {
      case PROTOCOL.UDP:
        this.broker = new UDPHelper(this.options.udpPort);
        break;

      case PROTOCOL.TCP:
        this.broker = new TCPHelper(this.options.tcpPort);
        break;

      case PROTOCOL.SERIAL:
        this.broker = new SerialPortHelper({
          path: this.options.serialPath,
          baudRate: this.options.baudRate || 57600,
        });
        break;

      default:
        throw new Error(`Unknown protocol ${this.options.protocol}`);
    }
  }

  onMessage(callback) {
    this.broker.onMessage((data) => {
      callback(data.buffer);
    });
  }

  send = (message) => {
    this.broker.send(message);
  };
}
