import * as mqtt from 'mqtt';

export default class MQTTHelper {
  constructor(mqtt, robotId) {
    this.mqtt = mqtt;
    this.robotId = robotId;
    this.sentBootMessage = false;

    this.#init();
  }

  #init() {
    this.mqttClient = mqtt.connect(this.mqtt.url, {
      username: this.mqtt.username,
      password: this.mqtt.password,
      keepalive: 10,
      will: {
        topic: `robots/${this.robotId}/shutdown`,
        payload: JSON.stringify({
          message: 'm1edge shutdown',
        }),
        qos: 0,
      },
    });

    this.mqttClient.on('connect', () => {
      console.log('[MQTT] connected');
    });
    this.mqttClient.on('reconnect', () => {
      console.log('[MQTT] reconnected');
    });
    this.mqttClient.on('close', () => {
      console.log('[MQTT] disconnected');
    });
    this.mqttClient.on('end', () => {
      console.log('[MQTT] end');
    });
    this.mqttClient.on('error', (error) => {
      console.log(`[MQTT] ${error}`);
    });

    this.mqttClient.subscribe(`robots/${this.robotId}/command`, { qos: 1 }, () =>
      console.log(`[MQTT] subscribe topic: robots/${this.robotId}/command`),
    );
  }

  onMessage(callback) {
    this.mqttClient.on('message', (topic, message) => {
      console.log('[MQTT → FCS]', topic, message.toString('hex'));
      callback(message);
    });
  }

  publish = (message) => {
    console.log('[FCS → MQTT]', message);

    if (!this.sentBootMessage) {
      const bootMessage = JSON.stringify({ message: 'm1edge boot' });
      this.mqttClient.publish(`robots/${this.robotId}/boot`, bootMessage);
      this.sentBootMessage = true;
    }
    this.mqttClient.publish(`robots/${this.robotId}/telemetry`, message);
  };
}
