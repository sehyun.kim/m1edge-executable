import { MavLinkPacketSplitter } from 'node-mavlink';
import { SerialPort } from 'serialport';

import heartbeat from '../utils/heartbeat.js';

export default class SerialPortHelper {
  constructor(options) {
    this.port = new SerialPort(options);
    this.send(heartbeat());
  }

  onMessage = (callback) => {
    const reader = this.port.pipe(new MavLinkPacketSplitter());
    reader.on('data', callback);

    this.port.on('error', this.#onError);
  };

  send = (buffer) => this.port.write(buffer, this.#onSendError);

  #onError = (error) => {
    console.log(`[SerialPort] OnError: ${error}`);
    if (this.port.isOpen) {
      this.port.close();
    }
    throw error;
  };

  #onSendError = (error) => {
    if (error) {
      console.log(`[SerialPort] OnSendError: ${error}`);
    }
  };
}
