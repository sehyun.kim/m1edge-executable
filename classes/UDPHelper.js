import { MavLinkPacketSplitter } from 'node-mavlink';
import stream from 'node:stream';
import dgram from 'node:dgram';

export default class UDPHelper {
  constructor(port) {
    this.input = new stream.PassThrough();
    this.server = dgram.createSocket({ type: 'udp4', reuseAddr: true });
    this.server.bind(port);
  }

  onMessage = (callback) => {
    const reader = this.input.pipe(new MavLinkPacketSplitter());
    reader.on('data', callback);

    this.server.on('listening', this.#onListen);
    this.server.on('message', (message, rinfo) => {
      if (!this.simIp) {
        this.simIp = rinfo.address;
        this.simPort = rinfo.port;
      }
      this.input.write(message, 'hex');
    });
    this.server.on('error', this.#onError);
  };

  send = (buffer) => this.server.send(buffer, this.simPort, this.simIp, this.#onSendError);

  #onListen = () => console.log(`[UDP] Listening on port ${this.server.address().port} `);

  #onError = (error) => {
    console.log(`[UDP] OnError: ${error}`);
    this.server.close();
  };

  #onSendError = (error) => {
    if (error) {
      console.log(`[UDP] OnSendError: ${error}`);
    }
  };
}
